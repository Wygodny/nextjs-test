import { find, filter } from 'lodash';
import { pubsub } from './subscriptions';
import models from './mongoose'

const PAGE_SIZE = 20

const resolveFunctions = {
  Query: {
    posts(root, { currentPage = 1 }) {
      const skip = PAGE_SIZE * (currentPage - 1)
      const limit = PAGE_SIZE
      console.log({skip, limit})
      return models.Post.find({})
        .skip(skip)
        .limit(limit)
        .exec()
    },
  },
  Subscription: {
    postUpvoted(post) {
      return models.Post.findOne({_id: post.id}).exec()  
    },
  },
  Author: {
    posts(author) {
      return models.Post.find({userId: author.id}).exec()  
    },
  },
  Post: {
    author(post) {
        return models.Author.findOne({_id: post.userId}).exec()  
    },
  },
};

export default resolveFunctions;