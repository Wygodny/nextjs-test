import { makeExecutableSchema } from 'graphql-tools';

import resolvers from './resolvers';

const schema = `
type Profile {
  name: String
}
type Author {
  id: String! # the ! means that every author object _must_ have an id
  profile: Profile
  posts: [Post] # the list of Posts by this author
}
type Post {
  id: String!
  title: String
  slug: String!
  author: Author
  body: String
}
# the schema allows the following query:
type Query {
  posts(currentPage: Int): [Post]
}
# this schema allows the following mutation:
type Mutation {
  upvotePost (
    postId: Int!
  ): Post
}
type Subscription {
  postUpvoted: Post
}
`;

export default makeExecutableSchema({
  typeDefs: schema,
  resolvers,
});