import mongoose from 'mongoose'

mongoose.Promise = global.Promise

mongoose.connect('mongodb://localhost:27017/produkcja2')

const Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
 
var Post = new Schema({
    _id: String,
    userId    : String,
    title     : String,
    body      : String,
    slug      : String,
})

var Author = new Schema({
    _id: String,
    profile: {
        name: String,
    }
})

const models = {
    Post: mongoose.model('blog_posts', Post),
    Author: mongoose.model('users', Author),
}

export default models