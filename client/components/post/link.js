import React from 'react'
import pathFor from '../../actions/pathFor'

export default class extends React.Component {
  postUrl(post) {
    return pathFor('post', post)
  }

  showPost(post) {
    this.props.url.push(this.postUrl(post))
  }
    
  render() {
    const { post } = this.props

    return (
      <a href={ this.postUrl(post) } onClick={ ()=> this.showPost(post) }>{ post.slug }</a>
    )
  }
}