import React from 'react'
import { style } from 'next/css'
import pathFor from '../../actions/pathFor'
import PostLink from './link'
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const PostList = function PostList(props) {
  const data = props.data || {}
  const { posts, loading } = data

  if (loading || !posts) {
    return <div>Loading</div>;
  } else {
    const { posts } = data
 
    return (
      <ul>
        {posts.map(post =>
          (
            <li key={post.id}>
              {post.title} by {' '}
              {post.author.profile.name} {' '}
            </li>
          )
        )}
      </ul>
    );
  }
}

PostList.propTypes = {
  data: React.PropTypes.object.isRequired
}

// export default PostList
export default graphql(gql`
  query allPosts ($currentPage: Int) {
    posts(currentPage: $currentPage) {
      id
      title
      author {
        id
        profile {
          name
        }
      }
    }
  }
`, {
    options: {
      variables: {
        currentPage: 1,
      }
    }
})(PostList);