const routes = {
    'post': function() {
        if (this && this.slug) {
            return `/post?slug=${ this.slug }`
        }
    }
}

export default (route, context) => {
    return routes[route] && routes[route].call(context)
}