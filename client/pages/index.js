import React from 'react'
import { style } from 'next/css'
import pathFor from '../actions/pathFor'
import PostList from '../components/post/list'
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';

export default class extends React.Component {
  constructor(...args) {
    super(...args)

    const networkInterface = createNetworkInterface({
      uri: 'http://localhost:8080/graphql'
    })

    this.client = new ApolloClient({
      networkInterface,
      dataIdFromObject: r => r.id,
    })

    this.state = {
      page: 1,
    }
  } 

  handleNext() {
    this.setState({page: this.state.page + 1})
  }

  handlePrev() {
    let page = this.state.page - 1
    if (!page) page = 1
    this.setState({page: page})
  }

  render() {
    return (
      <div>
        <h1>Posts</h1>
        <ApolloProvider client={this.client}>
          <PostList pageNumber={ this.state.page }/>
        </ApolloProvider>
        <button onClick={() => this.handlePrev()}>&lt;+1</button>
        <input readOnly value={ this.state.page } />
        <button onClick={ ()=>this.handleNext() }>+1&gt;</button>
      </div>
    )
  }
}